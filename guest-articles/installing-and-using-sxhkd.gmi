```
                          __       _______ _______ _______ ___ _______ ___     _______ _______
 .-----.--.--.-----.-----|  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |  |  |  -__|__ --|   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |___  |_____|_____|_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
 |_____|                          |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                                  |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                                  `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
```

# Installing And Using sxhkd
By Ronnie Nissan at April 21, 2020

sxhkd, short for, Simple X hot key daemon is, as the name implies a daemon for setting hot keys for X, which means it can set keybindings. sxhkd is in the default repos for most distros, you can also build it from source if it is not available in your distros default repos.

To install it on Arch Linux, type the following in the terminal:

```
sudo pacman -S sxhkd
```

On Void Linux, Type the following:

```
sudo xbps-install -S sxhkd
```

And on Ubuntu and Ubuntu-based distros:

```
sudo apt install sxhkd
```

Then you have to make config files for it.  From your home directory, run:

```
mkdir .config/sxhkd/
touch .config/sxhkd/sxhkdrc
```

Then in your .xinitrc (or any autostart script you use) you should add:

```
sxhkd -c $HOME/.config/sxhkd/sxhkdrc
```
https://github.com/baskerville/sxhkd
Note that adding the -c flag is to add a path to the config file, as the sxhkdrc is in the default location here, the -c flag and the path are not mandatory, but recommended.

Now if you open your sxhkdrc, it will be totally empty, because you just created it, and you can start adding things to it.

The syntax is simple, if you want to open firefox when pressing super + shift + f you'll add the following to your sxhkdrc file:

```
super + shift + f
    firefox
```

From the man page for sxhkd:

```
Each line of the configuration file is interpreted as so:
     • If it is empty or starts with #, it is ignored.
     • If it starts with a space, it is read as a command.
     • Otherwise, it is read as a hotkey.
```

But what makes sxhkd unique is it's ability to chain key cords. for example:

```
super + {_,shift + } f
    {pcmanfm,firefox}
```

In the above example, if you press, super + f, you'll spawn pcmanfm, but if you press super + shift + f you'll launch firefox. this makes the posibilities of setting keybinding wide. you can spawn applications, scripts, or run shell commands with sxhkd.

Now lets write a bad keybinding, then fix it to make it shorter and more elegant.

```
super + Left
    pamixer -d 5 #decrease the volume by 5
super + right
    pamixer -i 5 #increase the volume by 5
super + shift + m
    pamixer -t #toggle between mute and unmute
```

While the above keybindings will work correctly, we can right them more elegantly (and more cool too) as following:

```
super + {m,Left,Right}
    pamixer {-t,-d,-i} 5
```

You see how much cleaner the above keybinding is, and now if you want volume to increase by 10, all you have to do is to change it in one line only.

If you use i3wm you'll know that there is a long section of setting keybinding to switching to a workspaces and moving a window to a workspaces, as you can see below:

```
# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10
```

This is the default way i3wm config does it, now let's move it to sxhkdrc and use i3-msg to pass commands to i3 though sxhkd.

```
super + {_,shift +} {1-9}
    i3-msg {workspace,move container to workspace} {1-9}
```

So simply we are telling sxhkd, that if we press super + 5, run the command i3-msg workspace 5, which will take us to the 5th workspace but if we press super + shift + 5 run the commands i3-msg move container to workspace 5, which will move the selected window to the 5th workspace. and notice that you don't have to write {1,2,3,4,5,6,7,8,9} as sxhkd interprets {1-9} the same way.

As you can see from the above examples that sxhkd is a powerful and beautiful program. I put all my program launching, script running and non-wm related keybindings in my sxhkdrc so I have my default keybindings with me independent of the WM I'm using.

This tool has many more feature that I haven't covered here, like binding mouse buttons and using Emacs like keybindings. you'll be better to head to it's github page, or read the man pages to understand it's full capabilities.

## Links
=> https://github.com/baskerville/sxhkd Sxhkd on GitHub
