```
                          __       _______ _______ _______ ___ _______ ___     _______ _______
 .-----.--.--.-----.-----|  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |  |  |  -__|__ --|   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |___  |_____|_____|_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
 |_____|                          |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                                  |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                                  `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
```

# Vim Plugins Without a Plugin Manager
By Klaus-Dieter Schmatz at March 15, 2020

Vim and Neovim are incredibly extensible. New features can be implemented in Vimscript, packaged as a plugin, and published on Github. Vim users customize the editor to their liking by installing a set of plugins, sometimes so many that it becomes hard to keep an overview.

## Do You Need a Plugin Manager?
Most Vim users rely on a 3rd party plugin manager such as vim-plug, pathogen, Vundle, or NeoBundle. The plugin manager facilitates, to a certain extent, the tasks of installing, updating, and loading plugins.

It seems to be little known, though, that Vim and Neovim have builtin support for that. In fact, support for packages has been around for several years now. To find out more, type :help packages in an editor session.

Unless you have a particular reason to run a specific 3rd party plugin manager, the answer is “no, you don't need one”. Consider switching to the builtin solution and removing the legacy plugin manager.

This is what I have done recently. Read on for a brief overview.

## Setup
Initially, create a start directory. This is where the plugins will be located.

For Vim:

```
mkdir -p ~/.vim/pack/default/start
```

For Neovim:

```
mkdir -p ~/.local/share/nvim/site/pack/default/start
```

Note that default is just an arbitary name. You can choose something else if you prefer.

## Installing Plugins
To install a plugin, find out the URL of its repository. Most are on Github. Clone the repository into the start directory created above.

For example:

```
cd ~/.local/share/nvim/site/pack/default/start
git clone https://github.com/tpope/vim-sensible.git
git clone https://github.com/vim-airline/vim-airline.git
git clone https://github.com/morhetz/gruvbox.git
```

You may want to add the --depth option in the git clone command in order to truncate the history to a reasonable number of commits.

## Updating Plugins
To get the latest update foe a plugin, run git pull from within the respective repository. For instance:

```
cd ~/.local/share/nvim/site/pack/default/start/vim-sensible
git pull
```

To update all plugins, execute something like this:

```
for p in ~/.local/share/nvim/site/pack/default/start/*
do
    (echo $(basename "$p"); cd "$p"; git pull)
done
```

## Loading Plugins
This is the easiest part: plugins in the start directory are loaded automatically. There is no need to load anything explicitly in .vimrc or init.vim. Be sure to remove any Plug statements or similar artifacts that refer to the legacy plugin manager.
