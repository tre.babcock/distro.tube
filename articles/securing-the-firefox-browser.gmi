```
     __ __       _______ _______ _______ ___ _______ ___     _______ _______ 
 .--|  |  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
                |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
                                                                             
```

# Securing The Firefox Web Browser
By Derek Taylor at February 25, 2020

Most Firefox users think the Mozilla web browser has a sufficient amount of security and privacy out of the box. Compared to the other mainstream alternatives (like Chrome and Edge), Firefox is certainly better for privacy and security.

Why do people trust that Firefox more in terms of security and privacy than Google Chrome? The answer is simple–Firefox is free and open source software. Since the code is free available for anyone to inspect, researchers have audited Firefox many times over its history.

In contrast, Google Chrome along with Apple's Safari, Microsoft's Internet Explorer and Edge are all proprietary software. No one can audit the code. The very nature of being proprietary prevents them from ever being as secure as open sourced Firefox. But that doesn't mean that it can't be made more secure.

Of course, you can use a VPN service to browse the web, as that would solve all of your problems. But I'm going to ignore VPN's and instead show you how to make Firefox a more secure and privacy-focused browser by enabling or disabling the proper settings and installing the proper plugins.  

## Tracking Protection
Let's start by enabling tracking protection. Firefox can block trackers and maliciious scripts. There are three levels to tracking protection: Standard (the default), Strict and Custom. You can enable Strict protection but Firefox warns you that enabling this stronger setting may cause some websites to break.

Whether you enable Strict protection or stay with the Standard protection, I strongly advise setting the “Do Not Track” feature to ALWAYS. When you enable this option, it will allow Firefox to request websites to not try and track the user. Of course, websites may or may not comply. But there is no harm in enabling “Do Not Track”.

* Go to Preferences
* Privacy and Security
* Enhanced Tracking Protection
* Change from Standard to Strict, if you wish.
* Scroll down to “Do Not Track” and set to ALWAYS!
 
## Telemetry
Let's disable the telemetry that is built into Firefox by default. This telemetry collects user information to improve the browser's performance, but collecting user data is a privacy concern, so let's turn it off.

* Go to Preferences
* Privacy and Security
* Scroll down to Firefox Data Collection and Use
* Uncheck them all, if you wish.

## Change Your Search Engine
Firefox has switched its default search engine a couple of times in its history. A few years ago, Bing was the default. It is currently using Google as the default search engine, no doubt due to financial reasons. But neither Bing or Google are acceptable choices if privacy is a top pritority. Thankfully, Firefox offers DuckDuckGo as an option.

* Go to Preferences
* Search
* Default Search Engine
* Change it to DuckDuckGo.

## Two Extensions You Need
Go to Extensions and search for uBlock Origin. uBlock is an adblocker and is lightweight and easy on CPU and memory. Then search for Privacy Badger and install it. Privacy Badger is an anti-tracking addon that was created by the EFF. By using this extension, you are helping the EFF in the fight for privacy and digital rights.